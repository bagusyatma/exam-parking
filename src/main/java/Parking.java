import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Parking {
    String vehicle, inTime, outTime;
    long price;

    public void setVehicle(String inputVehicle){
        this.vehicle = inputVehicle;
    }

    public String getVehicle(){return this.vehicle; }

    public void setInTime(String inputInTime){
        this.inTime = inputInTime;
    }

    public void setOutTime(String inputOutTime){
        this.outTime = inputOutTime;
    }

    public String getParkingId(){
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime Formatter = LocalDateTime.parse(inTime, myParse);

        DateTimeFormatter parseYear = DateTimeFormatter.ofPattern("yy");
        String theYear = Formatter.format(parseYear);

        DateTimeFormatter parseMonth = DateTimeFormatter.ofPattern("MM");
        String theMonth = Formatter.format(parseMonth);

        DateTimeFormatter parseDate = DateTimeFormatter.ofPattern("ss");
        String theDate = Formatter.format(parseDate);
        String parkingId = "P" + theYear + theMonth + theDate + "001";

        return parkingId;
    }

    public void countPrice(){
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime In = LocalDateTime.parse(inTime, myParse);
        LocalDateTime Out = LocalDateTime.parse(outTime, myParse);

        String strIn = In.format(myParse);
        String strOut = Out.format(myParse);

        System.out.println("Masuk = " + strIn);
        System.out.println("Keluar = " + strOut);

        long hours = ChronoUnit.HOURS.between(In, Out);
        long hoursBetween = ChronoUnit.HOURS.between(In, Out) - 1;

        System.out.println("----------");
        System.out.println("Total Waktu = " + hours);
        System.out.println("----------");

        if (getVehicle().equalsIgnoreCase("Mobil")){
            this.price = hoursBetween * 2000 + 5000;
        }else if (getVehicle().equalsIgnoreCase("Motor")){
            this.price = hoursBetween * 1000 + 3000;
        }

        System.out.println(price);
    }

}
